"use client";

import { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
const BookList = () => {
  const tableStyle = {
    width: "100%",
    borderCollapse: "collapse",
    marginTop: "20px",
  };

  const thStyle = {
    padding: "10px",
    border: "1px solid #ddd",
    textAlign: "left",
    backgroundColor: "#f2f2f2",
    fontWeight: "bold",
  };

  const tdStyle = {
    padding: "10px",
    border: "1px solid #ddd",
    textAlign: "left",
  };

  const evenRowStyle = {
    backgroundColor: "#f9f9f9",
  };
  const [books, setBooks] = useState();
  const router = useRouter();
  const fetchBooks = async () => {
    try {
      console.log(
        'localStorage.getItem("token")',
        localStorage.getItem("token")
      );

      const response = await fetch("http://127.0.0.1:3000/api/books", {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      if (!response.ok) {
        throw new Error("Failed to fetch books");
      }
      const res = await response.json();
      setBooks(res.data);
    } catch (error) {
      console.error("Error fetching books:", error);
    }
  };

  useEffect(() => {
    fetchBooks();
  }, []);

  return (
    <div>
      <h2>Book List</h2>
      <table style={tableStyle}>
        <thead>
          <tr>
            <th style={thStyle}>Title</th>
            <th style={thStyle}>Author</th>
            <th style={thStyle}>Published Year</th>
          </tr>
        </thead>
        <tbody>
          {books?.map((book) => (
            <tr
              key={book.id}
              style={books.indexOf(book) % 2 === 0 ? evenRowStyle : {}}
            >
              <div
                style={tdStyle}
                onClick={() => router.push("/book/" + book.id)}
              >
                {book.title}
              </div>
              <td style={tdStyle}>{book.author}</td>
              <td style={tdStyle}>{book.year_of_publication}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default BookList;
