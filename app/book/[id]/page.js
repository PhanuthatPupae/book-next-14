"use client";

import { useParams } from "next/navigation";
import { useEffect, useState } from "react";

const BookById = () => {
  const containerStyle = {
    maxWidth: "600px",
    margin: "20px auto",
    padding: "20px",
    border: "1px solid #ddd",
    borderRadius: "8px",
    boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)",
    backgroundColor: "#fff",
    textAlign: "center",
  };

  const titleStyle = {
    fontSize: "24px",
    fontWeight: "bold",
    color: "#333",
    marginBottom: "10px",
  };

  const textStyle = {
    color: "#666",
    lineHeight: "1.6",
  };
  const [book, setBook] = useState();
  const params = useParams();

  const fetchBookById = async () => {
    try {
      const response = await fetch(
        `http://127.0.0.1:3000/api/books/${params.id}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        }
      );
      if (!response.ok) {
        throw new Error("Failed to fetch books");
      }
      const res = await response.json();
      console.log("s", res);
      setBook(res?.data);
    } catch (error) {
      console.error("Error fetching books:", error);
    }
  };

  useEffect(() => {
    console.log("first");
    fetchBookById();
  }, []);

  return (
    <div style={containerStyle}>
      <h1 style={titleStyle}>{book?.title}</h1>
      <p style={textStyle}>Author: {book?.author}</p>
      <p style={textStyle}>Published Year: {book?.year_of_publication}</p>
    </div>
  );
};

export default BookById;
